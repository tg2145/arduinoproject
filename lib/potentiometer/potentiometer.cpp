#include <constantVariables.hpp>
#include <Arduino.h>
#include <math.h>

/*
    this will be the thread function that is checking the potentiometer status
    and updates some variables when the potentiometer state changes
*/
void* potentiometerThreadFcn(ArduinoProjectConfig* config)
{
    static bool initialized =false;
    if(!initialized)
    {
        initialized = true;
    }

    //50ms - 5sec exponential mapping of power 2
    config->tick = ((4950*(pow((float)analogRead(A0)/1023.0, 2.0))) + 50);
    //you need to use this tick to update the interval of the timed action threads using .setInterval(newInterval)
    //if the tick speed hasnt changed since the last check dont call .setInterval
    //have it write the tick speed to display.message if the potentiometer is active
}