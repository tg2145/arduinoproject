#ifndef potentiometer_hpp
#define potentiometer_hpp

#include <constantVariables.hpp>
#include <Arduino.h>
#include <math.h>

//declare public functions here
void* potentiometerThreadFcn(ArduinoProjectConfig* config);

#include "potentiometer.cpp"

#endif