#ifndef display_hpp
#define display_hpp

#include <constantVariables.hpp>
#include <LiquidCrystal.h>

//declare public functions here
void* displayThreadFcn(ArduinoProjectConfig* config);

#include "display.cpp"

#endif