#include <constantVariables.hpp>
#include <LiquidCrystal.h>

#define TIME_TICKS_PER_UPDATE 5

/*
    this will be the thread function that is checking the led string
    and updates the screen with the string 
*/

const int rs = 13, e =12, d4 = 5, d5 = 6, d6 = 7, d7 = 8;
LiquidCrystal lcd(rs, e, d4, d5, d6, d7);

void* displayThreadFcn(ArduinoProjectConfig* config)
{
    static bool initialized =false;
    if(!initialized)
    {
        initialized = true;
    }
    delay(arduinoProjectConfig.tick); //remove this since you are now using timedAction to control the update intervals
    lcd.begin(0, 0);
        lcd.print(config->displayConfig.message);

    return nullptr;
}