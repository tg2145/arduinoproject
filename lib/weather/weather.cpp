#include <Arduino.h>
#include <stdio.h>
#include <constantVariables.hpp>
#include "RTClib.h"
#include <Wire.h>

RTC_DS1307 rtc;

/*
    this will be the thread function that is checking the temp and humidity
    and updates the variables with the measured values
*/
void weatherThreadFcn(ArduinoProjectConfig* config) //rename these files and functions since they aren't weather anymore
{
    static bool initialized = false;
    if(!initialized)
    {
        rtc.begin();
        initialized = true;
        rtc.adjust(DateTime((2000), (1), (1), (0), (0), (0)));

    }

    DateTime now = rtc.now();

    char message[100];
    memset(&message[0], 0, 16*(sizeof(char)));

    config->timeConfig.hour = now.hour();
    config->timeConfig.minute = now.minute();
    config->timeConfig.second = now.second();

    sprintf(&message[0], "%02d:%02d:%02d", (int)config->timeConfig.hour, (int) config->timeConfig.minute, (int) config->timeConfig.second);
    if(config->timeConfig.active)
    {
        config->serialMonitor.message = &message[0];
        config->displayConfig.message = &message[0];
    }

    return nullptr;
}