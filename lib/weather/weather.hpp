#ifndef weather_hpp
#define weather_hpp

#include <Arduino.h>
#include <stdio.h>
#include <constantVariables.hpp>
#include "RTClib.h"
#include <Wire.h>



//declare public functions here
void weatherThreadFcn(ArduinoProjectConfig* config);

#include "weather.cpp"

#endif