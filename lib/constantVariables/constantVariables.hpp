#ifndef constantVariables_hpp
#define constantVariables_hpp

//define constant variables here like pin numbers
//constant variable names like this should be in all CAPS

typedef struct 
{
    struct ButtonConfig
    {
        bool risingEdge;
        bool fallingEdge;
        bool active;
    };
    ButtonConfig buttonConfig;
    struct DisplayConfig
    {
        char* message;
    };
    DisplayConfig displayConfig;
    struct PotentiometerConfig
    {
        int voltage;
        bool active;
    };
    PotentiometerConfig potentiometerConfig;
    struct SerialMonitor
    {
        char* message;
    };
    SerialMonitor serialMonitor;
    struct TimeConfig
    {
        int hour;
        int minute;
        int second;
        bool active;
    };
    TimeConfig timeConfig;
    int tick;
    bool running;
} ArduinoProjectConfig;

static ArduinoProjectConfig arduinoProjectConfig;

#endif
