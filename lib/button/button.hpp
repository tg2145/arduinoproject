#ifndef button_hpp
#define button_hpp

#include <constantVariables.hpp>
#include <Arduino.h>

//declare public functions here
void* buttonThreadFcn(ArduinoProjectConfig* config);

#include "button.cpp"

#endif