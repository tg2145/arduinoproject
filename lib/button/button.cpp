#include <constantVariables.hpp>
#include <Arduino.h>
#define PIN 0 //put this in constantVariables and make the name more specific

bool current, previous; //make these static variables within the function so everything is contained in the function
/*
    this will be the thread function that is checking the button status
    and updates some variables when the button state changes
*/
void* buttonThreadFcn(ArduinoProjectConfig* config)
{
    static bool initialized = false;
    if(!initialized)
    {
        initialized = true;
        pinMode(PIN, INPUT);
    }

    while(config->running)
    {
        current = digitalRead(PIN);

        if(current != previous)
            {
                if(current == true) //if(current)
                {
                    config->buttonConfig.risingEdge == true;
                    config->buttonConfig.fallingEdge == false;
                }
                else
                {
                    config->buttonConfig.risingEdge == false;
                    config->buttonConfig.fallingEdge == true;
                }
                
            }
        else
            {
                config->buttonConfig.risingEdge == false;
                config->buttonConfig.fallingEdge == false;
            }
        previous = current;
        //at a rising edge, have it switch between activating the RTC time and potentiometer
    }
    return nullptr;
}