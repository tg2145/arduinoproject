#ifndef serialMonitor_hpp
#define serialMonitor_hpp

#include <constantVariables.hpp>
#include <Arduino.h>
#include <stdio.h>

//declare public functions here
void serialMonitorThreadFcn(ArduinoProjectConfig* config);

#include "serialMonitor.cpp"

#endif