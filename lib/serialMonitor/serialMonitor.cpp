#include <constantVariables.hpp>
#include <Arduino.h>

/*
    this will be the thread function that is writing the complete status of everything to serial monitor
    this will be for debugging
*/
void serialMonitorThreadFcn(ArduinoProjectConfig* config)
{
    static bool initialized = false;
    if(!initialized)
    {
        Serial.begin(9600);
        initialized = true;
    }
    
    //Serial.println(config->serialMonitor.message);
    Serial.println(config->tick);
    return;
}