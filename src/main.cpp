#include <Arduino.h>
#include <constantVariables.hpp>
#include <button.hpp>
#include <display.hpp>
#include <potentiometer.hpp>
#include <weather.hpp>
#include <serialMonitor.hpp>
#include <TimedAction.h>
#include "RTClib.h"
#include <Wire.h>
#include <LiquidCrystal.h>
#include <math.h>

void weatherThreadFcn_call(void); //this doesn't do anything since you are defining it in the next line. 
void weatherThreadFcn_call ()
{
  weatherThreadFcn(&arduinoProjectConfig);
}
void serialMonitorThreadFcn_call(void); //its like doing int x; x=5; instead of int x=5;
void serialMonitorThreadFcn_call()
{
  serialMonitorThreadFcn(&arduinoProjectConfig);
}
void potentiometerThreadFcn_call(void);
void potentiometerThreadFcn_call()
{
  potentiometerThreadFcn(&arduinoProjectConfig);
}
void displayThreadFcn_call(void);
void displayThreadFcn_call()
{
  displayThreadFcn(&arduinoProjectConfig);
}
void buttonThreadFcn_call(void);
void buttonThreadFcn_call(void)
{
  buttonThreadFcn(&arduinoProjectConfig);
}



unsigned long thread_time = 500;

  TimedAction weather_thread_action = TimedAction(thread_time,weatherThreadFcn_call); //the interval for this will be based on tick speed
  TimedAction serial_monitor_thread_action = TimedAction(thread_time,serialMonitorThreadFcn_call); //the interval for this will be based on tick speed
  TimedAction potentiometer_thread_action = TimedAction(thread_time, potentiometerThreadFcn_call); //make this interval small so we are regularly checking the potentiometer
  TimedAction display_thread_action = TimedAction(thread_time, displayThreadFcn_call); //the interval for this will be based on tick speed
  TimedAction button_thread_action = TimedAction(thread_time, buttonThreadFcn_call); //make this interval small so we are regularly checking the button
  //prove that with a tick speed of 5 seconds we can detect a quick button press in the middle of the tick

void setup() {
  arduinoProjectConfig.timeConfig.active = true;
}

void loop() {
  /*
  potentiometerThreadFcn(&arduinoProjectConfig);
  weatherThreadFcn(&arduinoProjectConfig);
  serialMonitorThreadFcn(&arduinoProjectConfig);
  displayThreadFcn(&arduinoProjectConfig);
  */

  weather_thread_action.check();
  serial_monitor_thread_action.check();
  potentiometer_thread_action.check();
  display_thread_action.check();
  button_thread_action.check();

}